# build zeppelin
      docker build -t zeppelin .
# Run zeppelin, opening port 8080
     docker run -ti -p 8080:8080 zeppelin bash
# idem, binding a local directory :
docker run -ti -v /c/Users/nfwc9163/Documents/spark/pushLogger:/home/zeppelin/pushLogger -p 8080:8080 zeppelin bash

# to stop/start/status inside docker image
     /opt/zeppelin-0.6.2-bin-netinst/bin/zeppelin-daemon.sh start
