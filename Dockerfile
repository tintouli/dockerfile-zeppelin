FROM java:alpine

# use bash
# RUN apk update
RUN apk add --no-cache bash

ENV BINARY=zeppelin-0.6.2-bin-netinst
ENV TGZ=${BINARY}.tgz

#download and install
RUN mkdir /opt
RUN wget -O /opt/${TGZ} http://ftp.halifax.rwth-aachen.de/apache/zeppelin/zeppelin-0.6.2/zeppelin-0.6.2-bin-netinst.tgz
RUN tar xvzf /opt/${TGZ}  -C /opt
RUN rm  /opt/${TGZ}
RUN /opt/${BINARY}/bin/install-interpreter.sh --name angular,flink,jdbc,md,python,shell
RUN /opt/${BINARY}/bin/zeppelin-daemon.sh start

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

# Environment variables
ENV ZEPPELIN_HOME /opt/${BINARY}
ENV PATH $PATH:$ZEPPELIN_HOME/bin
